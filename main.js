const images = document.querySelectorAll("img");

for (const image of images)
{
	if (image.src.endsWith('.svg'))
	{
		update(image);
	}
}

const observer = new MutationObserver(list =>
{
	for (const mutation of list)
	{
		for (const added of mutation.addedNodes)
		{
			if (added instanceof Image)
			{
				update(added);
			}
		}
	}
})

observer.observe(document.querySelector('body'), {attributes: true, childList: true, subtree: true});

function update(image)
{
	fetch(image.src).then(response => response.text()).then(text => image.outerHTML = text);
}